﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesExample
{
    //creating a delegate
    delegate double Operator(double n1, double n2);
    class Program
    {
        static double Add(double n1,double n2)
        {
            return n1 + n2;
        }
        static void Main(string[] args)
        {  //calling function by its name
           //Console.WriteLine(Add(10, 20));
           // calling by its address
           // Operator funcAddress = Add;
           // Console.WriteLine(funcAddress(10, 20));

            //a.create an object
            NassaCalculator nc = new NassaCalculator();
         //c.send the function to the delegate variable
           
            int result = nc.Calculate(showPrecent);
            Console.WriteLine("\n"+result);
        }
        //b.create a callback function
        static void showPrecent(int precent)
        {
            Console.Write("\r" + precent + "%"+"!!!");
        }
    }
}

